PDSF Slurm Partitions (queues), updated May 1, 2018

| partition  |  OS provider |  TotalCPUs | Time limit  | MaxJobPA <br>(per account) | MaxJobPU <br>(per user) | Relative priority | remarks |
|---|---|---|---|---|---|---| ---|
|shared-chos|chos|3352|2 days||| 0 | Share 94%
|alice|chos|3352|2 days||| 0 |  Share 94%
|realtime-chos |chos |128 |4 hours |50|| 0 |
|debug-chos |chos |128 |30 min | | 2 | 10 |
|long | shifter |384 | 2 days ||| 0
|short |shifter | 288 |5 hours |||0
|realtime |shifter |128 |4 hours |50||0 |share common hardware w/ dbg
|debug |shifter |128 |30 min ||2 |10
