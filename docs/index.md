# NERSC Technical Documentation

docs.nersc.gov is a resource with the technical details for users to
make effective use of [NERSC](https://nersc.gov)'s resources. For
center news and information visit
the [NERSC Home page](https://nersc.gov) and for interactive content
visit [MyNERSC](https://my.nersc.gov).

!!! tip 
	These pages are hosted from a 
	[git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
	[contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/master/CONTRIBUTING.md)
	are welcome!

## Quick links/ FAQ

 1. [Obtaining an Account](accounts/index.md#obtaining-an-account)
 1. [Building Software](programming/compilers/wrappers.md)
 1. [Running Jobs](jobs/index.md)
 1. [Managing Data](data/management.md)
 1. [Reset my password](accounts/index.md#forgotten-passwords)
