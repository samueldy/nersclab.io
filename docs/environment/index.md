# Environment

## NERSC User Enviroment

### Home Directories, Shells and Dotfiles

All NERSC systems use global home directories, which are are
pre-populated with shell initialization files (also known as dotfiles)
for all available shells. NERSC supports `bash`, `csh`, and
`tcsh` as login shells. Other shells (`ksh`, `sh`, and `zsh`) are also
available. The default shell at NERSC is bash.

#### Dotfiles

The standard dotfiles are symbolic links to read-only files that
NERSC controls. For each standard dotfile, there is a user-writeable
".ext" file.

!!! example
	For `csh` users `~/.login` and `~/.cshrc` are
	read-only. Customizations should be put in
	`~/.login.ext` and`~/.cshrc.ext`.

##### Fixing Dotfiles

Occasionally, a user will accidentally delete the symbolic links to
the standard dotfiles, or otherwise damage the dotfiles to the point
that it becomes difficult to do anything. In this case, the user
should run the command `fixdots`. This will recreate the original
dotfile configuration, after first saving the current configuration in
the directory `$HOME/KeepDots.timestamp` is a string that includes the
current date and time. After running `fixdots`, the user should
carefully incorporate the saved customizations into the newly-created
.ext files.

### Changing Default Login Shell

Use **NIM** to change your default login shell. Login, then select
**Change Shell** from the **Actions** pull-down menu.

## NERSC Modules Environment

NERSC uses the module utility to manage nearly all software. There are
two huge advantages of the module approach:

1. NERSC can provide many different versions and/or installations of a
   single software package on a given machine, including a default
   version as well as several older and newer version.
2. Users can easily switch to different versions or installations
   without having to explicitly specify different paths. With modules,
   the `MANPATH` and related environment variables are automatically
   managed.

### Module Command

The following is a list of commands available in the Modules
Environment tool available on Cori.

#### module help

To get a usage list of module options type the following (listing is
abbreviated):

```bash
module help

  Available Commands and Usage:

     + add|unload      modulefile [modulefile …]
     + rm|unload       modulefile [modulefile …]
     + switch          modulefile1 modulefile2
     + display         modulefile [modulefile …]
     + avail           path [path]
     + list
     + help            modulefile [modulefile …]
```

#### module list

```bash
module list
```

#### module avail

```bash
   # To get all available packages
   module avail

   # To know the availability of a specific software
   module avail netcdf

   # To know all packages that contain a substring, use -S flag.
   module avail -S netcdf
```

#### module display

To see what changes are made to your environment when a module is
loaded:

```bash
module display [modulefile]
```
or
```bash
module show [modulefile]
```

#### module load

This command will add one or more modulefiles to your current
environment.  It does so silently, but will throw errors if there are
any problems with the modulefile. If you load the generic name of a
module, you will get the default version. To load a specific version,
load the modulefile using the full specification.

```bash
module load [modulefile1][modulefile2]

# Load visit
module load visit

# Load visit version 2.1.2
module load visit/2.1.2
```

#### module unload

Unloads the specified modulefile from the user's environment. This
command will fail silently if the modulefile you specify is not
already loaded.

```bash
module unload [modulefile]
```

#### module swap

The modules environment allows you to *swap* between versions of
packages

```bash
module swap [old modulefile] [new modulefile]
```

### Accessing old Cray PE Software

Old Cray Developer Toolkits (CDT's) are removed after they are made
available on the system for one year. We encourage users to use the
new CDT's available on the system. However, if your workload depends
on a specific old CDT version, you can continuously access these
older CDT's from the archived copy at  `/global/common` by doing the
following:

```bash
module load pe_archive
```

### Creating a Custom Environment

You can modify your environment so that certain modules are loaded
whenever you log in. Put changes in one of the following files,
depending on your shell:

* `.cshrc.ext` or `.tcshrc.ext`
* `.bashrc.ext`

Users may have certain customizations that are appropriate for one
NERSC platform, but not for others. This can be accomplished by
testing the value of the environment variable `$NERSC_HOST`. For
example, on Cori the default programming environment is Intel
(PrgEnv-Intel). A C-shell user who wants to use the `GNU` programming
environment should include the following module command in their
`.cshrc.ext` file:

#### Cori

```bash
if ($NERSC_HOST == "cori") then
  module swap PrgEnv-intel PrgEnv-gnu
endif
```

### Install Your Own Customized Modules

You can create and install your own modules for your convenience or
for sharing software among collaborators. The module definition files
can be placed in the following locations:

* project directory
* your home directory
* available file system.

Make sure the **UNIX** file permissions grant access to all users who
want to use the software.

!!! warning
    Do not give write permissions to your home directory to anyone else.

As an example, we have modulefile named *myzlib* located in

`/global/project/projectdirs/mpccc/usg/modulefiles/cori`

To register this modulefile with our modules environment we run the
following commands:

```bash
nersc$ module use /global/project/projectdirs/mpccc/usg/modulefiles/cori
nersc$ module load myzlib/1.2.7
```

!!! note
	The `module use` command adds this new directory before
	other module search paths (defined as `$MODULEPATH`), so modules
	defined in this custom directory will have precedence if there are
	other modules with the same name in the module search paths. If
	you prefer to have the new directory added at the end of
	`$MODULEPATH`, use `module use -a` instead of `module use`.
